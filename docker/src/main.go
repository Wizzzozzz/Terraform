package main

import (
    "fmt"
    "log"
    "net/http"
    "os"
	"net"
)

const homepageEndPoint = "/"

// StartWebServer the webserver
func StartWebServer() {
    http.HandleFunc(homepageEndPoint, handleHomepage)
    port := os.Getenv("PORT")
    if len(port) == 0 {
        panic("Environment variable PORT is not set")
    }

    log.Printf("Starting web server to listen on endpoints [%s] and port %s",
        homepageEndPoint, port)
    if err := http.ListenAndServe(":"+port, nil); err != nil {
        panic(err)
    }
}

func handleHomepage(w http.ResponseWriter, r *http.Request) {
    urlPath := r.URL.Path
    log.Printf("Web request received on url path %s", urlPath)
	conn, error := net.Dial("udp", "8.8.8.8:80")  
	if error != nil {  
		fmt.Println(error)  
	}  

	defer conn.Close()  
	ipAddress := conn.LocalAddr().(*net.UDPAddr)  
	fmt.Println(ipAddress) 
    msg := ipAddress.String()
    _, err := w.Write([]byte(msg))
    if err != nil {
        fmt.Printf("Failed to write response, err: %s", err)
    }
}

func main() {
    StartWebServer()
}